<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

header('Content-Type: application/json');
if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    echo json_encode(array(
        'success' => false,
        'msg' => 'Method not acceptable',
    ));
    exit;
}

require_once 'handlers.php';
$checking = requestHandler($_POST);
if (sizeof($checking) > 0) {
    echo json_encode(array(
        'success' => false,
        'msg' => $checking,
    ));
    exit;
}

require_once 'db.php';
$con = connect();
require_once 'user.php';
$add = addUser($con, $_POST);
if (!$add) {
    echo json_encode(array(
        'success' => false,
        'msg' => 'error',
    ));
    disconnect($con);
    exit;
}

echo json_encode(array(
    'success' => true,
    'msg' => 'new user added',
));
exit;
