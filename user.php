<?php

function addUser($con, $data)
{
    $q = "INSERT INTO employees (name, email, age) VALUES ('" . $data['uname'] . "', '" . $data['uemail'] . "', '" . $data['uage'] . "')";
    if (!mysqli_query($con, $q)) {
        return false;
    }
    return true;
}

function getAll($con)
{
    $q = "SELECT * FROM employees";
    if (!$result = mysqli_query($con, $q)) {
        return false;
    }

    $data = [];
    for ($i = 0; $row = mysqli_fetch_array($result); $i++) {
        $data[$i] = $row;
    }
    return $data;
}

function getOne($con, $id)
{
    $q = "SELECT * FROM employees WHERE id ='$id'";
    if (!$result = mysqli_query($con, $q)) {
        return false;
    }
    $row = mysqli_fetch_array($result);
    return $row;

}

function deleteUser($con, $id)
{
    // checking if user is existing or not
    $checkUser = getOne($con, $id);
    if (empty($checkUser)) {
        return false;
    }
    // delete from DB
    $q = "DELETE FROM employees WHERE id = '$id'";
    if (!mysqli_query($con, $q)) {
        return false;
    }
    return true;
}

function editUser($con, $data)
{
    $id = $data['uid'];
    $checkUser = getOne($con, $data['uid']);
    if (empty($checkUser)) {
        return false;
    }
    $name = $data['uname'];
    $email = $data['uemail'];
    $age = $data['uage'];

    $q = "UPDATE employees SET name='$name', email='$email', age='$age' WHERE id = '$id'";
    if (!mysqli_query($con, $q)) {
        // echo mysqli_error($con);
        return false;
    }

    return true;
}
