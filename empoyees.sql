# ************************************************************
# Sequel Pro SQL dump
# Version 5438
#
# https://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.23)
# Database: test_db
# Generation Time: 2020-01-11 16:30:55 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table employees
# ------------------------------------------------------------

CREATE TABLE `employees` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `age` int(3) NOT NULL,
  `birthday` date DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;

INSERT INTO `employees` (`id`, `name`, `email`, `age`, `birthday`, `is_active`, `created_at`, `updated_at`)
VALUES
	(1,'33','teeest@gmail.com',27,NULL,1,NULL,'2019-08-24 20:46:24'),
	(2,'33','teeest@gmail.com',27,NULL,1,NULL,'2019-08-24 20:46:25'),
	(3,'33','teeest@gmail.com',27,NULL,1,NULL,'2019-08-24 20:46:32'),
	(4,'33','teeest@gmail.com',27,NULL,1,NULL,'2019-08-24 20:46:33'),
	(5,'33','teeest@gmail.com',27,NULL,1,NULL,'2019-08-24 20:47:24'),
	(6,'33','teeest@gmail.com',27,NULL,1,NULL,'2019-08-24 20:47:24'),
	(7,'33','teeest@gmail.com',27,NULL,1,NULL,'2019-08-24 20:48:16'),
	(8,'33','teeest@gmail.com',27,NULL,1,NULL,'2019-08-24 20:48:17'),
	(9,'33','teeest@gmail.com',27,NULL,1,NULL,'2019-08-24 20:49:42'),
	(10,'33','teeest@gmail.com',27,NULL,1,NULL,'2019-08-24 20:49:43'),
	(11,'33','teeest@gmail.com',27,NULL,1,NULL,'2019-08-24 20:49:50'),
	(12,'33','teeest@gmail.com',27,NULL,1,NULL,'2019-08-24 20:49:50'),
	(13,'33','teeest@gmail.com',27,NULL,1,NULL,'2019-08-24 20:50:32'),
	(14,'33','teeest@gmail.com',27,NULL,1,NULL,'2019-08-24 20:50:33'),
	(15,'33','teeest@gmail.com',27,NULL,1,NULL,'2019-08-24 20:51:24'),
	(16,'33','teeest@gmail.com',27,NULL,1,NULL,'2019-08-24 20:51:24'),
	(17,'33','teeest@gmail.com',27,NULL,1,NULL,'2019-08-24 20:51:39'),
	(18,'33','teeest@gmail.com',27,NULL,1,NULL,'2019-08-24 20:51:40'),
	(19,'33','teeest@gmail.com',27,NULL,1,NULL,'2019-08-24 20:56:39'),
	(20,'33','teeest@gmail.com',27,NULL,1,NULL,'2019-08-24 20:56:40'),
	(21,'dhfldfd','russulh.alsaffar@gmail.com',20,NULL,1,NULL,'2019-08-24 20:58:45'),
	(22,'zina','zina@gmail.com',20,NULL,1,NULL,'2019-08-24 20:59:30'),
	(23,'Russul Hazim Hameed','russulh.alsaffar@gmail.com',20,NULL,1,NULL,'2020-01-11 18:11:13'),
	(24,'Russul Hazim Hameed','russulh.alsaffar@gmail.com',20,NULL,1,NULL,'2020-01-11 18:11:15'),
	(25,'Russul Hazim Hameed','russulh.alsaffar@gmail.com',20,NULL,1,NULL,'2020-01-11 18:11:17'),
	(26,'Russul Hazim Hameed','russulh.alsaffar@gmail.com',20,NULL,1,NULL,'2020-01-11 18:11:18'),
	(27,'Russul Hazim Hameed','russulh.alsaffar@gmail.com',20,NULL,1,NULL,'2020-01-11 18:11:26');

/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
