<?php

// ini_set('display_errors', 1);
// error_reporting(E_ALL);

header('Content-Type: application/json');

require_once 'handlers.php';
$checking = requestHandler($_GET);
if (sizeof($checking) > 0) {
    echo json_encode(array(
        'success' => false,
        'msg' => $checking,
    ));
    exit;
}

require_once 'db.php';
$con = connect();
require_once 'user.php';
$delete_user = deleteUser($con, $_GET['uid']);
if (!$delete_user) {
    echo json_encode(array(
        'success' => false,
        'msg' => 'Something went wrong',
    ));
    disconnect($con);
    exit;
}

echo json_encode(array(
    'success' => true,
    'msg' => 'User with ID' . $_GET['uid'] . ' has been deleted',
));
exit;
